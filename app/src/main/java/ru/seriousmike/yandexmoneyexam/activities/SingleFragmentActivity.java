package ru.seriousmike.yandexmoneyexam.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.CookieManager;

import ru.seriousmike.yandexmoneyexam.R;
import ru.seriousmike.yandexmoneyexam.api.YandexAuthApi;

/**
 * Created by SeriousM on 26.09.2015.
 */
public abstract class SingleFragmentActivity extends AppCompatActivity {

	private static final String TAG = "sm_SingleFragment";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_single_fragment);
		FragmentManager fm = getSupportFragmentManager();
		Fragment fragment = fm.findFragmentById(R.id.fragmentContainer);
		Log.d(TAG, "fragment "+fragment);
		if(fragment==null) {
			fm.beginTransaction().add(
					R.id.fragmentContainer,
					getFragment()
			).commit();
		}
	}

	protected abstract Fragment getFragment();

	public void proceedToAuth() {
		YandexAuthApi.clearSession(SingleFragmentActivity.this);
		CookieManager.getInstance().removeAllCookie();
		Intent intent = new Intent(SingleFragmentActivity.this, LoginActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		finish();
	}
}
