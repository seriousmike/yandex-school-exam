package ru.seriousmike.yandexmoneyexam.activities;

import android.support.v4.app.Fragment;

import ru.seriousmike.yandexmoneyexam.fragments.AccountFragment;

public class AccountActivity extends SingleFragmentActivity {

	@Override
	protected Fragment getFragment() {
		return new AccountFragment();
	}
}
