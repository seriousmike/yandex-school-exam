package ru.seriousmike.yandexmoneyexam.activities;

import android.support.v4.app.Fragment;

import ru.seriousmike.yandexmoneyexam.fragments.LoginFragment;

public class LoginActivity extends SingleFragmentActivity {


	@Override
	protected Fragment getFragment() {
		return new LoginFragment();
	}
}
