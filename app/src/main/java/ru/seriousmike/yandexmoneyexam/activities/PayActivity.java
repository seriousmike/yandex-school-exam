package ru.seriousmike.yandexmoneyexam.activities;


import android.support.v4.app.Fragment;
import android.util.Log;

import ru.seriousmike.yandexmoneyexam.fragments.PayFragment;
import ru.seriousmike.yandexmoneyexam.helpers.Sequence;

public class PayActivity extends SingleFragmentActivity {

	private static final String TAG = "sm_Payment";

	public static final int REQUEST_CODE = Sequence.get();

	@Override
	protected Fragment getFragment() {
		Log.d(TAG, "getFragment");
		return new PayFragment();
	}
}
