package ru.seriousmike.yandexmoneyexam.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import ru.seriousmike.yandexmoneyexam.R;
import ru.seriousmike.yandexmoneyexam.api.YandexAuthApi;

public class SplashActivity extends AppCompatActivity {

	private static final String TAG = "sm_A_Splash";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_splash);

		if(checkAuth()) {
			proceed();
		} else {
			startAuth();
		}

	}

	private void proceed() {
		Log.d(TAG, "finishing activity");
		Intent i = new Intent(SplashActivity.this, AccountActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(i);
		finish();
	}

	private void startAuth() {
		Intent i = new Intent(SplashActivity.this, LoginActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(i);
		finish();
	}

	private boolean checkAuth() {
		return YandexAuthApi.hasAuthorized(getApplicationContext());
	}

}
