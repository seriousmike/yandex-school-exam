package ru.seriousmike.yandexmoneyexam.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.yandex.money.api.model.Operation;

import ru.seriousmike.yandexmoneyexam.R;

/**
 * Created by SeriousM on 29.09.2015.
 */
public class OperationAdapter extends ArrayAdapter<Operation> {

	// Constructor section -------------------------------------------------------------------------

	public OperationAdapter(Context context) {
		super(context, R.layout.li_operation);
	}

	// end section ---------------------------------------------------------------------------------

	// Adapter methods section ---------------------------------------------------------------------

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.li_operation, parent, false);
			ViewHolder holder = new ViewHolder();
			holder.amount = (TextView) convertView.findViewById(R.id.tvAmount);
			holder.status = (TextView) convertView.findViewById(R.id.tvStatus);
			holder.comment = (TextView) convertView.findViewById(R.id.tvComment);
			holder.title = (TextView) convertView.findViewById(R.id.tvTitle);
			convertView.setTag(holder);
		}

		Operation operation = getItem(position);
		ViewHolder holder = (ViewHolder) convertView.getTag();
		holder.title.setText( operation.title );
		holder.amount.setText( operation.amount.toString() );
		holder.status.setText( operation.status.name() );
		if(TextUtils.isEmpty(operation.comment)) {
			holder.comment.setVisibility(View.GONE);
		} else {
			holder.comment.setVisibility( View.VISIBLE );
			holder.comment.setText( operation.comment );
		}

		int colorResourceId;
		switch (operation.status) {
			case SUCCESS: colorResourceId = R.color.accentDark; break;
			case IN_PROGRESS: colorResourceId = R.color.text_secondary_alpha3; break;
			case REFUSED: colorResourceId = R.color.primaryDark; break;
			default: colorResourceId = R.color.text_primary;
		}
		holder.status.setTextColor( getContext().getResources().getColor(colorResourceId) );
		holder.amount.setTextColor( getContext().getResources().getColor(colorResourceId) );

		return convertView;
	}

	// end section ---------------------------------------------------------------------------------

	// Inner classes section -----------------------------------------------------------------------

	private class ViewHolder {
		TextView title;
		TextView status;
		TextView amount;
		TextView comment;
	}

	// end section ---------------------------------------------------------------------------------

}
