package ru.seriousmike.yandexmoneyexam.api;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.yandex.money.api.exceptions.InsufficientScopeException;
import com.yandex.money.api.exceptions.InvalidRequestException;
import com.yandex.money.api.exceptions.InvalidTokenException;
import com.yandex.money.api.methods.AccountInfo;
import com.yandex.money.api.methods.OperationHistory;
import com.yandex.money.api.methods.ProcessPayment;
import com.yandex.money.api.methods.RequestPayment;
import com.yandex.money.api.methods.params.P2pTransferParams;
import com.yandex.money.api.net.ApiRequest;
import com.yandex.money.api.net.DefaultApiClient;
import com.yandex.money.api.net.OAuth2Session;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.HashSet;

/**
 * Created by SeriousM on 26.09.2015.
 *
 * Чтобы не хранить client_id в коде приложения, всё взаимодействие я бы перенёс на сторону сервера.
 * В таком случае, нам нужно будет только хранить ссылку на наш сервер,
 * который сам будет полностью управляться с перенаправлением запросов и манипуляцией client_id.
 * Всё, что нам нужно будет делать - это получать access_token и сохранять его в приложении.
 * Client_id был бы известен только серверу и в таком случае, никакой опасности
 * скомпрометировать идентификатор приложения по декомпиляции не было бы.
 *
 * Но так как это экзаменационное задание на программирование под андроид,
 * то будет более честно по минимуму использовать серверные backend-ы
 * и перенести всю логику в андроид-приложение, благополучно плевав на безопасность и здравый смысл.
 *
 * В боевых условиях я бы перенёс все эти настройки на сторону сервера.
 */
public class YandexAuthApi {

	private static final String TAG = "sm_YAA";

	private static final boolean IS_TEST = false;

	private static final String TEST_HOST = "https://demomoney.yandex.ru";
	private static final String PROD_HOST = "https://money.yandex.ru";
	private static final String AUTH_URL_PROD = "https://m.money.yandex.ru/oauth/authorize";
	private static final String AUTH_URL_TEST = "https://m.demomoney.yandex.ru/oauth/authorize";
	private static final String URI_TOKEN = "/oauth/token";
	private static final String URI_REVOKE = "/api/revoke";

	static final String APP_CLIENT_ID = "F61516965C2BD66E002FCC114B0B273B893DE0F81CD048117E5A5F4E711EAA66";
	static final String APP_NAME = "YandexSchoolExamLapaevSecondTry";
	public static final String REDIRECT_URL = "http://www.lapaevm.ru/yandexmoneyschool/";

	private static final String PARAM_SCOPE = "scope";
	private static final String PARAM_REDIRECT_URI = "redirect_uri";
	private static final String PARAM_CLIENT_ID = "client_id";
	private static final String PARAM_RESPONSE = "response_type";
	private static final String PARAM_GRANT_TYPE = "grant_type";
	private static final String PARAM_ACCESS_TOKEN = "access_token";

	private static final String RESPONSE_CODE = "code";
	private static final String GRANT_TYPE = "authorization_code";
	private static final String SCOPE_STRING = "account-info operation-history payment-p2p money-source(\"wallet\")";

	private static final String PREF_NAME = "yandex_config";
	private static final String VAL_TOKEN = "token";

	private static YandexAuthApi sInstance;
	private String mToken;
	private OAuth2Session mSession;


	// Constructors and initializers section -------------------------------------------------------

	private YandexAuthApi() {
	}

	private void setSession(Context context) {
		initToken(context);
		if(mToken == null) {
			throw new IllegalStateException("No token found");
		}

		if(mSession == null) {
			mSession = new OAuth2Session(new DefaultApiClient(APP_CLIENT_ID));
			mSession.setAccessToken(mToken);
		}
	}

	public static YandexAuthApi startSession(Context context) throws IllegalStateException {
		if(sInstance == null) {
			sInstance = new YandexAuthApi();
			sInstance.setSession(context);
		}
		return sInstance;
	}

	public static YandexAuthApi getSession() {
		return sInstance;
	}

	private void initToken(Context context){
		if(mToken == null) {
			mToken = getAccessToken(context);
		}
	}

	public static void clearSession(Context context) {
		String savedToken = getAccessToken(context);
		if(savedToken != null) {
			revokeToken(savedToken);
		}
		context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).edit().remove(VAL_TOKEN).commit();
		if(sInstance != null) {
			sInstance.mSession = null;
			sInstance.mToken = null;
			sInstance = null;
		}
	}

	// end section ---------------------------------------------------------------------------------


	// Request methods section ---------------------------------------------------------------------

	public AccountInfo getAccountInfo() throws InvalidTokenException {
		return makeRequest(new AccountInfo.Request());
	}

	public OperationHistory getHistory() throws InvalidTokenException {
		OperationHistory.Request.Builder builder = new OperationHistory.Request.Builder();
		builder.setDetails(false).setRecords(100);

		return makeRequest( builder.createRequest() );
	}

	public RequestPayment sendPaymentRequest(String to, String comment, String amount, int expire) throws InvalidTokenException {
		P2pTransferParams.Builder builder = new P2pTransferParams.Builder(to)
				.setMessage(comment)
				.setComment(comment)
				.setLabel(APP_NAME)
//				.setCodepro(true)
				.setAmount(new BigDecimal(amount))
				.setExpirePeriod(expire);

		return makeRequest( RequestPayment.Request.newInstance(builder.build()) );
	}

	public ProcessPayment sendPaymentProcess(String requestId) throws InvalidTokenException {
		return makeRequest( new ProcessPayment.Request(requestId) );
	}

	private <T> T makeRequest(ApiRequest<T> request) throws InvalidTokenException {
		try {
			return mSession.execute(request);
		} catch (InsufficientScopeException e) {
			Log.e(TAG, "InsufficientScope?", e);
		} catch (InvalidRequestException e) {
			Log.e(TAG, "InvalidRequest?", e);
		} catch (IOException e) {
			Log.e(TAG, "IOException? No internete", e);
		}
		return null;
	}

	// end section ---------------------------------------------------------------------------------

	// Auth methods section ------------------------------------------------------------------------

	private static void revokeToken(String token) {
		OkHttpClient client = new OkHttpClient();
		MediaType form = MediaType.parse("application/x-www-form-urlencoded; charset=utf-8");
		RequestBody body = RequestBody.create( form, "");
		Request request = new Request.Builder()
				.url(getHost()+URI_REVOKE)
				.addHeader("Authorization", "Bearer "+token)
				.post(body)
				.build();
		client.newCall(request).enqueue(new Callback() {
			@Override
			public void onFailure(Request request, IOException e) {
				// empty
			}

			@Override
			public void onResponse(Response response) throws IOException {
				// empty
			}
		});
	}

	public static String getHost() {
		return IS_TEST ? TEST_HOST : PROD_HOST;
	}

	public static String getAuthUrl() {
		return IS_TEST ? AUTH_URL_TEST : AUTH_URL_PROD;
	}

	public static String getAuthParams() {
		return new StringBuilder().append(PARAM_CLIENT_ID).append("=").append(APP_CLIENT_ID)
				.append("&").append(PARAM_SCOPE).append("=").append(getUrlEncoded(SCOPE_STRING))
				.append("&").append(PARAM_REDIRECT_URI).append("=").append(getUrlEncoded(REDIRECT_URL))
				.append("&").append(PARAM_RESPONSE).append("=").append(RESPONSE_CODE).toString();
	}

	private static String getTokenParams(String code) {
		return new StringBuilder().append(PARAM_CLIENT_ID).append("=").append(APP_CLIENT_ID)
				.append("&").append(RESPONSE_CODE).append("=").append(code)
				.append("&").append(PARAM_REDIRECT_URI).append("=").append(getUrlEncoded(REDIRECT_URL))
				.append("&").append(PARAM_GRANT_TYPE).append("=").append(GRANT_TYPE).toString();
	}

	private static String getUrlEncoded(String stringToEncode) {
		try {
			return URLEncoder.encode(stringToEncode, "utf-8");
		} catch (UnsupportedEncodingException e) {
			return stringToEncode;
		}
	}

	public static String getTokenByRedirectedCode(String redirectedUrl) {

		final String code = getCodeFromRedirectedUrl(redirectedUrl);
		String token = null;
		if(code==null || code.isEmpty()) {
			return null;
		}

		try {
			final String response = sendTokenRequest(code);
			if(response!=null) {
				token = parseToken(response);
			}
		} catch (IOException e) {
			Log.e("sm_H", e.toString());
		}

		return token;
	}

	private static String getCodeFromRedirectedUrl(String redirectedUrl) {
		final String codeParam = "?" + RESPONSE_CODE + "=";
		if( redirectedUrl==null || !redirectedUrl.contains(REDIRECT_URL) || !redirectedUrl.contains(codeParam) ) {
			return null;
		}

		final int pos = redirectedUrl.indexOf(codeParam) + codeParam.length();
		return redirectedUrl.substring(pos);
	}

	private static String sendTokenRequest(String code) throws IOException{
		OkHttpClient client = new OkHttpClient();
		MediaType form = MediaType.parse("application/x-www-form-urlencoded; charset=utf-8");
		RequestBody body = RequestBody.create( form, getTokenParams(code));
		Request request = new Request.Builder()
				.url(getHost()+URI_TOKEN)
				.post(body)
				.build();
		Response response = client.newCall(request).execute();
		if(response.isSuccessful()) {
			return response.body().string();
		} else {
			return null;
		}
	}

	private static String parseToken(String jsonResponse) {
		JsonParser jp = new JsonParser();
		JsonElement je = jp.parse(jsonResponse);
		if(je.getAsJsonObject().has(PARAM_ACCESS_TOKEN)) {
			return je.getAsJsonObject().get(PARAM_ACCESS_TOKEN).getAsString();
		}
		return null;
	}

	public static boolean hasAuthorized(Context context) {
		return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).getString(VAL_TOKEN, null) != null;
	}

	static String getAccessToken(Context context) {
		return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).getString(VAL_TOKEN, null);
	}

	public static void saveAccessToken(Context context, String token) {
		context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
				.edit().putString(VAL_TOKEN, token).commit();
	}

	// end section ---------------------------------------------------------------------------------

}
