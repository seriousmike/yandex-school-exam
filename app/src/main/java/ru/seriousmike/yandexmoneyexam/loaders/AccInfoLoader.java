package ru.seriousmike.yandexmoneyexam.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.yandex.money.api.exceptions.InvalidTokenException;
import com.yandex.money.api.methods.AccountInfo;
import com.yandex.money.api.methods.OperationHistory;
import com.yandex.money.api.model.Operation;

import java.util.List;

import ru.seriousmike.yandexmoneyexam.api.YandexAuthApi;

/**
 * Created by SeriousM on 28.09.2015.
 */
public class AccInfoLoader extends AsyncTaskLoader<AccInfoLoader.Data> {

	private static final String TAG = "sm_L_AccInfo";

	public AccInfoLoader(Context context) {
		super(context);
	}

	@Override
	public Data loadInBackground() {
		Log.i(TAG, "loadInBg");
		Data data = new Data();
		YandexAuthApi.startSession(getContext());
		try {
			data.setAccountInfo(YandexAuthApi.getSession().getAccountInfo());
			OperationHistory history = YandexAuthApi.getSession().getHistory();
			if(data.getAccountInfo()!=null && history != null) {
				data.setHistory( history.operations );
				data.setNextRecord(history.nextRecord);
				data.setStatus(Status.SUCCESS);
				Log.i(TAG, "accinfo: "+ data.getAccountInfo());
				Log.i(TAG, "history: " + YandexAuthApi.getSession().getHistory() );
			} else {
				data.setStatus(Status.ERROR);
			}
		} catch (InvalidTokenException e) {
			data.setStatus(Status.AUTH_NEEDED);
		}
		return data;
	}

	public static class Data {

		private Status mStatus;
		private List<Operation> mHistory;
		private AccountInfo mAccountInfo;
		private String mNextRecord;


		public Status getStatus() {
			return mStatus;
		}

		public void setStatus(Status status) {
			mStatus = status;
		}

		public List<Operation> getHistory() {
			return mHistory;
		}

		public void setHistory(List<Operation> history) {
			mHistory = history;
		}

		public AccountInfo getAccountInfo() {
			return mAccountInfo;
		}

		public void setAccountInfo(AccountInfo accountInfo) {
			mAccountInfo = accountInfo;
		}

		public String getNextRecord() {
			return mNextRecord;
		}

		public void setNextRecord(String nextRecord) {
			mNextRecord = nextRecord;
		}
	}

	public enum Status {
		ERROR, SUCCESS, AUTH_NEEDED
	}
}
