package ru.seriousmike.yandexmoneyexam.helpers;

/**
 * Created by SeriousM on 28.09.2015.
 */
public class Sequence {

	private static int sSequence = 0;

	private Sequence() {
		// empty private constructor
	}

	public static int get() {
		return sSequence++;
	}

}
