package ru.seriousmike.yandexmoneyexam.helpers;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;

import ru.seriousmike.yandexmoneyexam.R;

/**
 * Created by SeriousM on 29.09.2015.
 */
public class ProjHelper {

	public static boolean isInternetAvailable(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		return ( networkInfo!=null && networkInfo.isConnectedOrConnecting());
	}

	public static void showNetworkErrorDialog(Context context, @Nullable DialogInterface.OnClickListener onRetryClick) {
		showCustomErrorDialog(
				context,
				onRetryClick,
				R.string.dialog_title_network_error,
				(onRetryClick==null ? R.string.dialog_message_internet_error : R.string.dialog_message_internet_error_retry)
		);
	}

	public static void showCustomErrorDialog(
			Context context,
			@Nullable DialogInterface.OnClickListener onRetryClick,
			@StringRes int title,
			@StringRes int text
	) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		if(onRetryClick!=null) {
			builder.setPositiveButton(R.string.dialog_btb_retry, onRetryClick);
		}

		builder.setTitle(title)
				.setMessage(text)
				.setCancelable(false)
				.setNegativeButton(
						R.string.dialog_btn_cancel,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}
						}
				).create().show();
	}

}
