package ru.seriousmike.yandexmoneyexam.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.yandex.money.api.model.Operation;


import ru.seriousmike.yandexmoneyexam.R;
import ru.seriousmike.yandexmoneyexam.activities.PayActivity;
import ru.seriousmike.yandexmoneyexam.adapters.OperationAdapter;
import ru.seriousmike.yandexmoneyexam.helpers.ProjHelper;
import ru.seriousmike.yandexmoneyexam.helpers.Sequence;
import ru.seriousmike.yandexmoneyexam.loaders.AccInfoLoader;


/**
 * Created by SeriousM on 28.09.2015.
 *
 * В данном фрагменте предполагается вовсю использовать БД для кэширования списка операций,
 * но, к сожалению, из-за кошмарнейшей нехватки времени вынужден полностью миновать работы с БД.
 * Надеюсь, это не станет ихлишне критичным моментом.
 * Во вступительном задании в МШЯД вся работа с БД была реализована ручками на основе курсоров и
 * концепции DBHelper-ов. Надеюсь, в том задании я показал, что умею управляться с БД =)
 */
public class AccountFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<AccInfoLoader.Data> {

	private static final String TAG = "sm_F_List";

	private static final int LOADER_ID = Sequence.get();

	private View mProgressBar;

	private TextView mBalance;
	private TextView mAccount;
	private FloatingActionButton mFAB;

	private ArrayAdapter<Operation> mAdapter;

	private AccInfoLoader.Data mLastData;

	// Private object members section --------------------------------------------------------------

	private DialogInterface.OnClickListener mDialogListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			refreshData();
		}
	};

	// end section ---------------------------------------------------------------------------------

	// Fragment methods section --------------------------------------------------------------------

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		setHasOptionsMenu(true);
		Log.i(TAG, "onCreate");
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View layout = inflater.inflate(R.layout.fr_account, container, false);

		mProgressBar = layout.findViewById(R.id.progressBar);

		mBalance = (TextView) layout.findViewById(R.id.tvBalance);
		mAccount = (TextView) layout.findViewById(R.id.tvAccount);
		mFAB = (FloatingActionButton) layout.findViewById(R.id.fabAdd);
		mFAB.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getActivity(), PayActivity.class);
				startActivityForResult(i, PayActivity.REQUEST_CODE);
			}
		});
		setFabOffset();

		final ListView listView = (ListView) layout.findViewById(R.id.lvOperations);
		if(mAdapter == null) {
			mAdapter = new OperationAdapter(getActivity());
		}
		listView.setAdapter(mAdapter);
		listView.setEmptyView(layout.findViewById(R.id.emptyView));
		//TODO: per-page loading
		fillData();

		getLoaderManager().initLoader(LOADER_ID, null, this);
		Log.i(TAG, "saveInstanceState " + savedInstanceState);
		if(savedInstanceState==null || mLastData==null) {
			refreshData();
		}

		return layout;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_account, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.menuLogout:
				proceedToAuth();
				return true;
			default: return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == PayActivity.REQUEST_CODE && resultCode == Activity.RESULT_OK) {
			refreshData();
		}
	}

	// end section ---------------------------------------------------------------------------------

	// Private methods section ---------------------------------------------------------------------

	private void setFabOffset() {
		mFAB.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
			@Override
			public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
				RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mFAB.getLayoutParams();
				lp.topMargin = -(mFAB.getHeight() / 2);
				mFAB.setLayoutParams(lp);
			}
		});
	}

	private void refreshData() {
		mProgressBar.setVisibility(View.VISIBLE);
		getLoaderManager().getLoader(LOADER_ID).forceLoad();
	}

	private void fillData() {
		if(mLastData!=null) {
			mAccount.setText(mLastData.getAccountInfo().account);
			mBalance.setText(mLastData.getAccountInfo().balance.toString());
			mAdapter.clear();
			mAdapter.addAll(mLastData.getHistory());
			mAdapter.notifyDataSetChanged();
			mProgressBar.setVisibility(View.GONE);
		}
	}

	// end section ---------------------------------------------------------------------------------

	// Loader methods section ----------------------------------------------------------------------

	@Override
	public Loader<AccInfoLoader.Data> onCreateLoader(int id, Bundle args) {
		Log.i(TAG, "onCreateLoader id "+id+" / "+LOADER_ID);
		return new AccInfoLoader(getActivity());
	}

	@Override
	public void onLoadFinished(Loader<AccInfoLoader.Data> loader, AccInfoLoader.Data data) {
		Log.d(TAG, "got data " + data);

		switch (data.getStatus()) {
			case SUCCESS:
				mLastData = data;
				fillData();
				break;
			case AUTH_NEEDED:
				proceedToAuth();
				break;

			case ERROR:
				if(ProjHelper.isInternetAvailable(getActivity())) {
					ProjHelper.showCustomErrorDialog(
							getActivity(),
							mDialogListener,
							R.string.dialog_title_request_error,
							R.string.dialog_text_request_error
					);
				} else {
					ProjHelper.showNetworkErrorDialog(getActivity(), mDialogListener);
				}
				break;
		}

	}

	@Override
	public void onLoaderReset(Loader<AccInfoLoader.Data> loader) {
		Log.d(TAG, "onLoaderReset");
		// do nothing?
	}

	// end section ---------------------------------------------------------------------------------
}
