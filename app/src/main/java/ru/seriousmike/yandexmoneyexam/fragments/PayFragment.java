package ru.seriousmike.yandexmoneyexam.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.yandex.money.api.exceptions.InvalidTokenException;
import com.yandex.money.api.methods.BaseProcessPayment;
import com.yandex.money.api.methods.BaseRequestPayment;
import com.yandex.money.api.methods.ProcessPayment;
import com.yandex.money.api.methods.RequestPayment;
import com.yandex.money.api.model.Error;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Locale;

import ru.seriousmike.yandexmoneyexam.R;
import ru.seriousmike.yandexmoneyexam.api.YandexAuthApi;
import ru.seriousmike.yandexmoneyexam.helpers.ProjHelper;


/**
 * Created by SeriousM on 26.09.2015.
 */
public class PayFragment extends BaseFragment {

	private static final String TAG = "sm_F_Pay";

	private static final float COMMISSION = 1.005f;

	private static final long PAYMENT_OFFSET_MILLIS = 3000;

	private View mContainer;
	private View mProgressBar;
	private EditText mEtToPay;
	private EditText mEtSumm;
	private EditText mEtReceiver;
	private EditText mEtComment;
	private EditText mEtCode;
	private EditText mEtDays;
	private Button mButton;

	private boolean mIgnoreTextChangeFlag = false;

	private RequestPayment mAnswer;
	private ProcessPayment mResult;

	private ProcessTask mPayTask;

	private DecimalFormat mDecimalFormat;

	// Listeners section ---------------------------------------------------------------------------

	private View.OnFocusChangeListener mOnFocusFormatter = new View.OnFocusChangeListener() {
		@Override
		public void onFocusChange(View v, boolean hasFocus) {
			if(!hasFocus) {
				final EditText et = (EditText) v;
				float value;
				try {
					value = mDecimalFormat.parse(et.getText().toString().replace(',','.')).floatValue();
				} catch (ParseException e) {
					Log.e(TAG, et.getText()+" cannot parse to float", e);
					value = 0;
				}
				mIgnoreTextChangeFlag = true;
				et.setText( String.format("%.2f", value) );
				mIgnoreTextChangeFlag = false;
			}
		}
	};

	private DialogInterface.OnClickListener mDialogListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			mButton.callOnClick();
		}
	};

	// end section ---------------------------------------------------------------------------------

	// Fragment methods section --------------------------------------------------------------------

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		setRetainInstance(true);
		ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
		if(actionBar!=null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
		}

		DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.ENGLISH);
		mDecimalFormat = new DecimalFormat();
		mDecimalFormat.setDecimalFormatSymbols(symbols);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mContainer = inflater.inflate(R.layout.fr_payment_form, container, false);

		mEtToPay = (EditText) mContainer.findViewById(R.id.etToPay);
		mEtSumm = (EditText) mContainer.findViewById(R.id.etSumm);
		mEtComment = (EditText) mContainer.findViewById(R.id.etComment);
//		mEtCode = (EditText) mContainer.findViewById(R.id.etCode);
		mEtDays = (EditText) mContainer.findViewById(R.id.etDays);
		mEtReceiver = (EditText) mContainer.findViewById(R.id.etReceiver);

		mButton = (Button) mContainer.findViewById(R.id.btnGo);
		mProgressBar = mContainer.findViewById(R.id.progressBar);

		setViewListeners();

		return mContainer;
	}


	@Override
	public void onResume() {
		super.onResume();
		processResult();
		processAnswer();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				getActivity().setResult(Activity.RESULT_CANCELED);
				getActivity().finish();
				return true;

			default:
				return super.onOptionsItemSelected(item);
		}
	}

	// end section ---------------------------------------------------------------------------------

	// Private methods section ---------------------------------------------------------------------

	private void setViewListeners() {
		mEtSumm.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				Log.i(TAG, "etSumm beforeTextChanged "+s);
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				if(!mIgnoreTextChangeFlag) {
					mIgnoreTextChangeFlag = true;
					Log.i(TAG, "etSumm afterTextChanged " + s);

					float amount, amount_due;
					try {
						amount = mDecimalFormat.parse(s.toString().replace(',', '.')).floatValue();
						amount_due = amount * COMMISSION;
					} catch (ParseException e) {
						amount_due = 0;
					}
					mEtToPay.setText( String.format("%.2f", amount_due) );
					mIgnoreTextChangeFlag = false;
				}
			}
		});

		mEtSumm.setOnFocusChangeListener(mOnFocusFormatter);

		mEtToPay.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				if(!mIgnoreTextChangeFlag) {
					mIgnoreTextChangeFlag = true;
					Log.i(TAG, "etToPay afterTextChanged " + s);
					float amount_due, amount;
					try {
						amount_due = mDecimalFormat.parse(s.toString().replace(',','.')).floatValue();
						amount = amount_due / COMMISSION;
					} catch (ParseException e) {
						amount = 0;
					}
					mEtSumm.setText(String.format("%.2f", amount));
					mIgnoreTextChangeFlag = false;
				}
			}
		});

		mEtToPay.setOnFocusChangeListener(mOnFocusFormatter);

		mButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if(hasErrors()) {
					return;
				}

				RequestPaymentWrapper wrapper = new RequestPaymentWrapper();
				try {
					wrapper.amount = String.format("%.2f",Float.parseFloat(mEtToPay.getText().toString().replace(',','.')));
				} catch (NumberFormatException e) {
					wrapper.amount = "0.00";
				}

				try {
					wrapper.expire = Integer.parseInt(mEtDays.getText().toString());
				} catch (NumberFormatException e) {
					wrapper.expire = 1;
				}

				wrapper.comment = mEtComment.getText().toString();
				wrapper.to = mEtReceiver.getText().toString();

				new RequestTask().execute(wrapper);
				mProgressBar.setVisibility(View.VISIBLE);
			}
		});
	}

	private void processAnswer() {
		if(mAnswer == null) {
			return;
		}

		Log.d(TAG, "RequestPayment " + mAnswer);
		if(mAnswer.status == BaseRequestPayment.Status.SUCCESS ) {
			Log.d(TAG, "All good");
			Snackbar.make(mContainer, R.string.snackbar_payment_successfull, Snackbar.LENGTH_LONG).setAction(R.string.snackbar_payment_cancel, new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mProgressBar.setVisibility(View.GONE);
					if(mPayTask != null) {
						mPayTask.cancel(true);
					}
				}
			}).show();
			mPayTask = new ProcessTask();
			mPayTask.execute(mAnswer.requestId);
		} else if(mAnswer.status == BaseRequestPayment.Status.REFUSED) {
			showRequestErrorDialog(mAnswer);
		}

		mAnswer = null;
	}

	private void showRequestErrorDialog(RequestPayment requestPayment) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
				.setTitle(R.string.dialog_title_request_error)
				.setCancelable(false)
				.setNegativeButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
		builder.setMessage(mAnswer.error.code);
		if(requestPayment.error == Error.EXT_ACTION_REQUIRED) {
			final String extActionUri = requestPayment.extActionUri;
			builder.setPositiveButton(R.string.dialog_btn_do_something, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent i = new Intent(Intent.ACTION_VIEW);
					i.setData(Uri.parse(extActionUri));
					startActivity(i);
				}
			});
		}
		builder.create().show();
		mProgressBar.setVisibility(View.GONE);
	}

	private void processResult() {
		Log.d(TAG, "Result " + mResult);
		if(mResult != null) {
			if(mResult.status == BaseProcessPayment.Status.SUCCESS) {
				getActivity().setResult(Activity.RESULT_OK);
				Toast.makeText(getActivity(), R.string.payment_successful, Toast.LENGTH_SHORT).show();
				getActivity().finish();
			} else {
				new AlertDialog.Builder(getActivity())
						.setTitle(R.string.dialog_title_request_error)
						.setCancelable(false)
						.setNegativeButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}
						})
						.setMessage(mAnswer.error.code)
						.create().show();
				mProgressBar.setVisibility(View.GONE);
			}
			mResult = null;
		}
	}

	private boolean hasErrors() {
		int errors = 0;

		errors += checkEmptiness(mEtReceiver);
		errors += checkEmptiness(mEtDays);
		errors += checkEmptiness(mEtToPay);
		errors += checkEmptiness(mEtSumm);

		return errors > 0;
	}

	private int checkEmptiness(EditText et) {
		if(et.getText().length()==0) {
			et.setError( getString(R.string.et_error_required) );
			return 1;
		}
		return 0;
	}

	// end section ---------------------------------------------------------------------------------

	// Tasks section -------------------------------------------------------------------------------

	private class RequestTask extends AsyncTask<RequestPaymentWrapper,Void,RequestPayment> {

		@Override
		protected RequestPayment doInBackground(RequestPaymentWrapper... params) {
			RequestPayment answer = null;
			try {
				answer = YandexAuthApi.getSession().sendPaymentRequest(
						params[0].to,
						params[0].comment,
						params[0].amount.replace(',','.'),
						params[0].expire
				);
			} catch (InvalidTokenException e) {
				proceedToAuth();
			}
			return answer;
		}

		@Override
		protected void onPostExecute(RequestPayment answer) {
			mAnswer = answer;
			if(isVisible()) {
				if(answer == null && !ProjHelper.isInternetAvailable(getActivity())) {
					ProjHelper.showNetworkErrorDialog(getActivity(), mDialogListener);
				} else if (answer == null) {
					ProjHelper.showCustomErrorDialog(
							getActivity(),
							mDialogListener,
							R.string.dialog_title_request_error,
							R.string.dialog_text_request_error
					);
				}
				processAnswer();
			}
		}
	}

	private class ProcessTask extends AsyncTask<String,Void,ProcessPayment> {

		@Override
		protected ProcessPayment doInBackground(String... params) {
			Log.d(TAG, "offset");
			try {
				Thread.sleep(PAYMENT_OFFSET_MILLIS);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			Log.d(TAG, "offset gone!");
			if(isCancelled()) {
				Log.d(TAG, "cancelled");
				return null;
			}

			ProcessPayment answer = null;
			try {
				answer = YandexAuthApi.getSession().sendPaymentProcess(params[0]);
			} catch (InvalidTokenException e) {
				proceedToAuth();
			}
			return answer;
		}

		@Override
		protected void onPostExecute(ProcessPayment answer) {
			mResult = answer;
			if(isVisible()) {
				if(answer == null && !ProjHelper.isInternetAvailable(getActivity())) {
					ProjHelper.showNetworkErrorDialog(getActivity(), mDialogListener);
				} else if (answer == null) {
					ProjHelper.showCustomErrorDialog(
							getActivity(),
							mDialogListener,
							R.string.dialog_title_request_error,
							R.string.dialog_text_request_error
					);
				}
				processResult();
			}
		}
	}

	private class RequestPaymentWrapper {
		String to;
		String comment;
		String amount;
		int expire;
	}

	// end section ---------------------------------------------------------------------------------

}
