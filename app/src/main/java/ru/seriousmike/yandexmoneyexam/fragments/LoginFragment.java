package ru.seriousmike.yandexmoneyexam.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import ru.seriousmike.yandexmoneyexam.R;
import ru.seriousmike.yandexmoneyexam.api.YandexAuthApi;
import ru.seriousmike.yandexmoneyexam.activities.AccountActivity;
import ru.seriousmike.yandexmoneyexam.helpers.ProjHelper;

/**
 * Created by SeriousM on 27.09.2015.
 */
public class LoginFragment extends BaseFragment {
	private static final String TAG = "sm_F_Login";

	private View mProgressBar;

	private TokenTask mTask;
	private WebView mWebView;

	// Fragment methods section --------------------------------------------------------------------

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View layout = inflater.inflate(R.layout.activity_login, container, false);

		Log.i(TAG, "onCreateView "+savedInstanceState);

		mProgressBar = layout.findViewById(R.id.progressBar);

		final ProgressBar loadingBar = (ProgressBar) layout.findViewById(R.id.loadingIdentifier);

		mWebView = (WebView) layout.findViewById(R.id.webView);
		mWebView.getSettings().setDomStorageEnabled(true);
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
		mWebView.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				Log.i(TAG, "url " + url);
				// как уже было пояснено во вступительном комментарии к классу YandexAuthApi
				// вся работа выполняется на стороне мобильного приложения только из-за того,
				// что это задание по программированию мобильного приложения, а не рабочей системы.
				// в идеале здесь мы должны получать только окончательный токен.
				if(url.contains(YandexAuthApi.REDIRECT_URL) && mTask == null) {
					mProgressBar.setVisibility(View.VISIBLE);
					mTask = new TokenTask(getActivity(), url);
					mTask.execute();
					Toast.makeText(getActivity(), R.string.status_retrieving_token, Toast.LENGTH_SHORT).show();
				}
				return false;
			}

			@Override
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
				super.onReceivedError(view, errorCode, description, failingUrl);
				checkInternet();
			}

		});

		mWebView.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView webView, int progress) {
				if(progress == 100) {
					loadingBar.setVisibility(View.GONE);
				} else {
					loadingBar.setVisibility(View.VISIBLE);
					loadingBar.setProgress(progress);
				}
			}
		});

		loadStartPage();
		return layout;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_login, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.menuRestart:
				new AlertDialog.Builder(getActivity())
						.setTitle(R.string.dialog_title_restart)
						.setMessage(R.string.dialog_text_restart)
						.setNegativeButton(
								R.string.dialog_btn_cancel,
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										dialog.dismiss();
									}
								})
						.setPositiveButton(R.string.dialog_btb_retry, mDialogListener)
						.create().show();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	// end section ---------------------------------------------------------------------------------

	// Private methods section ---------------------------------------------------------------------

	private void checkInternet() {
		if(!ProjHelper.isInternetAvailable(getActivity())) {
			ProjHelper.showNetworkErrorDialog(getActivity(), mDialogListener);
		}
	}

	private void loadStartPage() {
		// как уже было пояснено во вступительном комментарии к классу YandexAuthApi
		// вся работа выполняется на стороне мобильного приложения только из-за того,
		// что это задание по программированию мобильного приложения, а не рабочей системы
		mWebView.postUrl(YandexAuthApi.getAuthUrl(), YandexAuthApi.getAuthParams().getBytes());
	}

	// end section ---------------------------------------------------------------------------------

	// Listeners section ---------------------------------------------------------------------------

	private DialogInterface.OnClickListener mDialogListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			loadStartPage();
		}
	};

	// end section ---------------------------------------------------------------------------------

	// Tasks section -------------------------------------------------------------------------------

	private class TokenTask extends AsyncTask<Void,Void,Void> {

		private Context mContext;
		private String mRedirectedUrl;
		private boolean mSuccess;
		private boolean mDone;

		public TokenTask(Context c, String redirectedUrl) {
			mContext = c.getApplicationContext();
			mRedirectedUrl = redirectedUrl;
		}

		public boolean isSuccess() {
			return mSuccess;
		}

		public boolean isDone() {
			return mDone;
		}

		@Override
		protected Void doInBackground(Void... params) {
			String token = YandexAuthApi.getTokenByRedirectedCode(mRedirectedUrl);
			if(token != null) {
				mSuccess = true;
				YandexAuthApi.saveAccessToken(mContext, token);
			}
			mDone = true;
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			if(!mSuccess) {
				mTask = null;
				Toast.makeText(mContext, R.string.status_token_error, Toast.LENGTH_SHORT).show();
				if(isVisible()) {
					loadStartPage();
					mProgressBar.setVisibility(View.GONE);
				}
			} else if(isVisible()) {
				Intent i = new Intent(getActivity(), AccountActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(i);
				getActivity().finish();
			}
		}
	}

	// end section ---------------------------------------------------------------------------------

}
