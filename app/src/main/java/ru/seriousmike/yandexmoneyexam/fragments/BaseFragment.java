package ru.seriousmike.yandexmoneyexam.fragments;

import android.support.v4.app.Fragment;

import ru.seriousmike.yandexmoneyexam.activities.SingleFragmentActivity;

/**
 * Created by SeriousM on 29.09.2015.
 */
public class BaseFragment extends Fragment {

	protected SingleFragmentActivity getBaseActivity() {
		return (SingleFragmentActivity) getActivity();
	}

	protected void proceedToAuth() {
		getBaseActivity().proceedToAuth();
	}

}
